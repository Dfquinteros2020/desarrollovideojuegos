﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    public static gameManager Instance { get; private set; }
    public InputControllerR2 InputController { get; private set; }


    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        InputController = GetComponentInChildren<InputControllerR2>();
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
