﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carro : MonoBehaviour
{
    //public Transform centertOfMass;
    //public WheelCollider wheelColliderLeftFront;
    //public WheelCollider wheelColliderRightFront;
    //public WheelCollider wheelColliderLeftBack;
    //public WheelCollider wheelColliderRightBack;

    //public Transform wheelLeftFront;
    //public Transform wheelRightFront;
    //public Transform wheelLeftBack;
    //public Transform wheelRightBack;


    public float motorTorque=100f;
    public float maxSteer = 20f;

    public float Steer { get; set; }
    public float Throttle { get; set; }



    private Rigidbody _rigidbody;

    private rueda[] ruedas;


    void Start()
    {
        ruedas = GetComponentsInChildren<rueda>(); //pone a todos los hijos con el componente rueda dentro del array rueda
        _rigidbody = GetComponent<Rigidbody>();
        //_rigidbody.centerOfMass = centertOfMass.localPosition;
    }

    void FixedUpdate()
    {
        //wheelColliderLeftBack.motorTorque = Input.GetAxis("Vertical") * motorTorque;
        //wheelColliderRightBack.motorTorque = Input.GetAxis("Vertical") * motorTorque;
        //wheelColliderLeftFront.steerAngle = Input.GetAxis("Horizontal") * maxSteer;
        //wheelColliderRightFront.steerAngle = Input.GetAxis("Horizontal") * maxSteer;
    }

    void Update() {

        //var pos = Vector3.zero;
        //var rot = Quaternion.identity;

        //wheelColliderLeftFront.GetWorldPose(out pos, out rot);
        //wheelLeftFront.position = pos;
        //wheelLeftFront.rotation = rot;

        //wheelColliderRightFront.GetWorldPose(out pos, out rot);
        //wheelRightFront.position = pos;
        //wheelRightFront.rotation = rot*Quaternion.Euler(0,180,0);

        //wheelColliderLeftBack.GetWorldPose(out pos, out rot);
        //wheelLeftBack.position = pos;
        //wheelLeftBack.rotation = rot;

        //wheelColliderRightBack.GetWorldPose(out pos, out rot);
        //wheelRightBack.position = pos;
        //wheelRightBack.rotation = rot * Quaternion.Euler(0, 180, 0);

        Steer = gameManager.Instance.InputController.SteerInput;
        Throttle = gameManager.Instance.InputController.ThrottleInput;

        foreach (var rueditas in ruedas) {
            rueditas.SteerAngle = Steer * maxSteer;
            rueditas.Torque = Throttle * motorTorque;
        }

    }
    
}
